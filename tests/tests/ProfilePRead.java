import java.io.*;
import java.util.*;
import java.net.*;

import org.apache.hadoop.fs.*;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.util.*;

public class ProfilePRead {

  public static double TestPRead(FSDataInputStream fdis, int len, 
                                 byte[] buffer, long round, long maxOffset) 
  throws IOException {
    Random rand = new Random(System.currentTimeMillis());
    long startTime = System.currentTimeMillis();
    long offset = 0;
    for (int i = 0; i < round; ++i) {
      offset = rand.nextLong() % maxOffset;
      fdis.read(offset, buffer, 0, len);
    }
    double dur = System.currentTimeMillis() - startTime;
    return dur / round;
  }

  public static void main(String args[]) {
    try {
      //uri is something like: "hdfs://localhost:9000"
      String uri = args[0];
      String fn = args[1];

      Configuration conf = new Configuration();
      FileSystem hdfs = FileSystem.get(new URI(uri), conf);
      Path file = new Path(uri + "/test/" + fn);
      if (!hdfs.exists(file)) {
        throw new IOException(uri + "/test/" + fn 
            + " does not exist!");
      }

      long totalLen = hdfs.getFileStatus(file).getLen();

      FSDataInputStream fdis = 
        hdfs.open(file);

      int maxShift = 24; // 16MB
      int curLen = 1 << 10;

      int MB = 1 << 20;

      for (int i = 0; i < maxShift; ++i) {
        int round = Math.max(MB / curLen, 50);
        round = Math.min(round, 1000);
        byte [] buffer = new byte[curLen + 10];
        double delay = TestPRead(fdis, curLen, buffer, round, totalLen - curLen - 1); 
        System.out.println(curLen + "," + delay + "," + ((1<<27) * delay/curLen));
        curLen = curLen << 1;
      }

      fdis.close();
      hdfs.close();
    } catch (Exception e) {
      System.out.println(e.getMessage());
      e.printStackTrace();
    }
  }
}
