import java.util.ArrayList;
import java.util.Arrays;

public class PReadDelayEstimator {
  
  public static final double INFTY = 1e30;

  private static long [] sizes = null;
  private static double [] delays = null;
  private static boolean initialized = false;

  public synchronized static void init(String strProfile) {
    if (initialized) {
      return;
    }
    if (null == strProfile) {
      throw new IllegalStateException("Shen Li: null estimator profile");
    }
    String [] strPairs = strProfile.split(";");
    sizes = new long[strPairs.length];
    delays = new double[strPairs.length];
    int idx = 0;
    for (String strPair : strPairs) {
      String [] items = strPair.split(",");
      if (2 != items.length) {
        throw new IllegalStateException("Shen Li: "
            + "invalid strProfile " + strProfile);
      }
      // TODO: sort is not sorted
      sizes[idx] = Long.parseLong(items[0]);
      delays[idx++] = Double.parseDouble(items[1]);
    }
    initialized = true;
  }

  public static boolean isInitialized() {
    return initialized;
  }

  public static boolean isTooLarge(long size) {
    return size > sizes[sizes.length - 1];
  }

  public static int getMaxSize() {
    return (int)sizes[sizes.length - 1];
  }

  public static double estimateDelay(long size) {
    if (null == sizes || null == delays) {
      throw new IllegalStateException("Shen Li: "
          + "cannot estimate delay, profile not initialized");
    }

    int idx = Arrays.binarySearch(sizes, size);
    if (idx < 0) {
      // the idx of the last element that is smaller than size
      idx = -idx - 2;
    }

    if (idx < 0) {
      // size smaller than the first
      return delays[0];
    } else if (idx == sizes.length - 1) {
      // size larger than all
      return INFTY;
    } else {
      long left = size - sizes[idx];
      long right = sizes[idx + 1] - size;
      double diff = delays[idx + 1] - delays[idx];
      return delays[idx] + diff * left / (left + right);
    }
  }

  public static void main(String args[]) {
    init("1024,10;2048,11;4096,12;8192,15;16384,20;");
    System.out.println(estimateDelay(900));
    System.out.println(estimateDelay(2048));
    System.out.println(estimateDelay(10000));
    System.out.println(estimateDelay(20000));
  }
}
