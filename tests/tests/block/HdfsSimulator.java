import java.util.Random;
import java.util.LinkedList;

import java.io.IOException;
import java.net.*;

import org.apache.hadoop.fs.*;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.util.*;

public class HdfsSimulator {

  private static final String profile = 
    "1024,8.89753052622818;" +
    "2048,8.90399214590238;" +
    "4096,8.91691538525077;" +
    "8192,8.94276186394755;" +
    "16384,8.99445482134112;" +
    "32768,9.09784073612826;" +
    "65536,9.30461256570253;" +
    "131072,9.71815622485106;" +
    "262144,10.54524354314814;" +
    "524288,13.6115225359761;" +
    "1048576,15.8061628880442;" +
    "2097152,20.1954435921803;" +
    "4194304,28.9740050004525;" +
    "8388608,46.5311278169970;" +
    "16777216,81.6453734500860;" +
    "33554432,151.8738647162639";
    
  //"1024,10;2048,11;4096,12;8192,15;16384,20;32768,30;65536,50;131072,90";

  public static LinkedList<Long> ranges2blocks(LinkedList<Range> ranges,
                                      int blockSize, int rowSize) {
    LinkedList<Long> ret = new LinkedList<Long>();
    long startOffset = 0, endOffset = 0;
    for (Range range: ranges) {
      startOffset = range.getStart() * rowSize / blockSize;
      endOffset = range.getEnd() * rowSize / blockSize;
      for (long offset = startOffset; offset <= endOffset; ++offset) {
        if (ret.size() <= 0 || offset != ret.get(ret.size() - 1)) {
          ret.add(new Long(offset));
        }
      }
    }
    return ret;
  }


  public static Rectangle getRandRect(double xx, double yy, 
                                      GeoContext gc, long seed) {
    Random rand = new Random();
    rand.setSeed(seed);
    double ax = gc.getMaxX() * rand.nextDouble();
    double ay = gc.getMaxY() * rand.nextDouble();

    double angle = 2 * Math.PI * rand.nextDouble();
    Rectangle rect = new Rectangle(ax, ay, xx, yy);
    return rect.rotate(angle);
  }


  public static double testDelay(FSDataInputStream fdis, 
                                 LinkedList<Range> aggBlocks,
                                 int blockSize, boolean isAgg,
                                 long fileSize, byte[] buffer)
  throws IOException {
    double totalDelay = 0;
    double oneBlockDelay = PReadDelayEstimator.estimateDelay(blockSize);
    long startOffset = 0;
    long endOffset = 0;
    long startTime = 0;
    long endTime = 0;
    for (Range range : aggBlocks) {
      startOffset = range.getStart() * blockSize;
      endOffset = (range.getEnd() + 1) * blockSize;
      if (isAgg) {
        if (endOffset > fileSize) {
          throw new IllegalStateException("Exceed file limit");
        }
        startTime = System.currentTimeMillis();
        fdis.read(startOffset, buffer, 0, (int)(endOffset - startOffset));
        totalDelay += (System.currentTimeMillis() - startTime);
      } else {
        startTime = System.currentTimeMillis();
        for (long offset = startOffset; offset < endOffset; offset += blockSize) {
          fdis.read(offset, buffer, 0, blockSize);
        }
        totalDelay += (System.currentTimeMillis() - startTime);
      }
    }

    return totalDelay;
  }

  public static void main(String argv[]) {
    double maxX = 50000L;
    double maxY = 50000L;

    // 5000 5000 25 20 10 4096 512 4000 True hdfs://tarekc60.cs.illinois.edu:9000 /profile/NYCTLC_TPEP_122008_012010.txt 

    double setXX = Double.parseDouble(argv[0]);
    double setYY = Double.parseDouble(argv[1]);
    long maxResolution = Long.parseLong(argv[2]);
    long reqResolution = Long.parseLong(argv[3]);
    int testNum = Integer.parseInt(argv[4]);
    int blockSize = Integer.parseInt(argv[5]);
    int rowSize = Integer.parseInt(argv[6]);
    long maxCount = Long.parseLong(argv[7]);
    boolean isAgg = Boolean.parseBoolean(argv[8]);
    String uri = argv[9];
    String strPath = argv[10];

    double ax, ay;
    try {

      // connect to HDFS
      Configuration conf = new Configuration();
      FileSystem hdfs = FileSystem.get(new URI(uri), conf);

      Path file = new Path(uri + strPath);
      if (!hdfs.exists(file)) {
        throw new IOException(uri + strPath 
            + " does not exist!");
      }

      long totalLen = hdfs.getFileStatus(file).getLen();

      FSDataInputStream fdis = 
        hdfs.open(file);

      double tileXLen = maxX / (1L << maxResolution);
      double tileYLen = maxY / (1L << maxResolution);

      GeoContext gc = new GeoContext(maxResolution, maxX, maxY);
      GeoEncoding mge = new MooreGeoEncoding(gc);

      GeoRequestParser mgrp = new BraidQuadTreeGeoRequestParser(mge);
      
      double [] delays = new double[testNum];
      PReadDelayEstimator.init(profile);
      byte[] buffer = new byte[PReadDelayEstimator.getMaxSize() + 100];
      for (int i = 0 ; i < testNum; ++i) {
        System.out.println("Test Num: " + i);
        // TODO: make sure it allows customized resolution
        RectangleGeoRequest gr = 
          new RectangleGeoRequest(getRandRect(setXX, setYY, gc, System.currentTimeMillis()),
          reqResolution);
        System.out.println("11111");
        LinkedList<Range> ranges = mgrp.getScanRanges(gr);
        System.out.println("22222");
        LinkedList<Long> blocks = ranges2blocks(ranges, blockSize, rowSize);
        System.out.println("33333 " + blocks.size());
        LinkedList<Range> aggBlocks =  
          BlockAggregator.aggregate(blocks, blockSize, maxCount);
        System.out.println("44444 " + aggBlocks.size());
        delays[i] = testDelay(fdis, aggBlocks, blockSize, isAgg,
                              totalLen, buffer);
      }

      double meanDelay = 0;
      double minDelay = 999999999999.0;
      double maxDelay = 0;
      for (double delay : delays) {
        meanDelay += delay;
        if (delay < minDelay) {
          minDelay = delay;
        }
        if (delay > maxDelay) {
          maxDelay = delay;
        }
      }
      meanDelay = meanDelay / testNum;
      System.out.println(meanDelay + ", " + minDelay + ", " + maxDelay + ", ");
      
      fdis.close();
      hdfs.close();
    } catch (Exception ex) {
      System.out.println(ex.getMessage());
      ex.printStackTrace();
    }
  }
}
